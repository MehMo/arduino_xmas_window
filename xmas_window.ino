//www.elegoo.com
//2016.12.8

// define pins

int ledPinA = 3;
int ledPinB = 5;
int onBtn = 13;
int counter = 0;
int ledBrtA = -255;
int ledBrtB = 128;
void setup()
{
  pinMode(ledPinA, OUTPUT);
  pinMode(ledPinB, OUTPUT);
  pinMode(onBtn, INPUT_PULLUP);
  //Serial.begin(9600);
}



// main loop
void loop()
{
  
#define delayTime 10 // fading time between colors

if(digitalRead(onBtn) == LOW){
counter ++;
delay(250);
}

if(ledBrtA == 255){
  ledBrtA = -255;
}

if(ledBrtB == 255){
  ledBrtB = -255;
}

if(counter % 2 == 0 ){
  ledBrtA++;

int sign = 1;
if( ((double) rand() / (RAND_MAX)) < .5 ){
  sign = -1;
}

  ledBrtB = ledBrtB + sign * (rand() % 5);
    analogWrite(ledPinA,    abs(ledBrtA));
    analogWrite(ledPinB,    abs(ledBrtB));
    delay(delayTime);
  
}else{  
  analogWrite(ledPinA,    0);
  analogWrite(ledPinB,    0);
  ledBrtA = -255;
  ledBrtB = 0;
}

  //Serial.print(counter);
}
